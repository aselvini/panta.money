const cacheName = 'v2-'+Math.floor(Math.random() * 100);

const baseAssets = [
  'app.js',
  '/'
];

const cachedPaths = [
  /outlays\/list/,

];

self.addEventListener('install', event => {
  console.log('worker installed ', event);

  event.waitUntil(
    cacheAssets(cacheName, baseAssets)
      .then(() => self.skipWaiting())
  );
});

self.addEventListener('activate', event => {
  console.log('worker activated ', event);

  event.waitUntil(
    purgeCaches(cacheName)
  );
});

self.addEventListener('fetch', event => {
  console.log('worker fetch ', event.request.url);
  event.respondWith(fetchOrGetFromCache(event.request));
});

const fetchOrGetFromCache = async (request) => {
  const response = await fetch(request)
    .then(async res => {
      if (cachedPaths.some(path => path.test(request.url))) {
        await caches.open(cacheName).then(cache => cache.put(request, res.clone()));
      }
      return res;
    })
    .catch(async (err) => {
      console.log('FAILED ', err, request);
      const cache = await caches.open(cacheName).then(cache => cache.match(request));
      return cache;
    });

  return response;
};

const cacheAssets = (cacheName, assets) => {
  return caches
    .open(cacheName)
    .then(cache => cache.addAll(assets));
}

const purgeCaches = (currentCache) => {
  const purge = (names) => {
    return names.map(async name => {
      if (currentCache !== name) {
        await caches.delete(name);
      }
    });
  };

  return caches
    .keys()
    .then(purge);
}