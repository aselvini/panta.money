import {
  stringToDate,
  dateToString,
  isDefined,
  isDate
} from '@/lib/utils';

describe('utils', () => {
  describe('date utils', () => {
    it('stringToDate', () => {
      const separator = '/';
      const date = stringToDate(`01${separator}02${separator}2003`, separator);

      expect(date.getDate()).toEqual(1);
      expect(date.getMonth()).toEqual(1);
      expect(date.getFullYear()).toEqual(2003);
    });

    it('dateToString', () => {
      const separator = '/';
      const str = dateToString(new Date(2003, 1, 1), separator);

      expect(str).toEqual(`01${separator}02${separator}2003`);
    });
  });

  describe('validation utils', () => {
    it('isDefined', () => {
      expect(isDefined('a')).toBeTruthy();
      expect(isDefined(5)).toBeTruthy();
      expect(isDefined({})).toBeTruthy();
      expect(isDefined([])).toBeTruthy();
      expect(isDefined(null)).toBeFalsy();
      expect(isDefined(undefined)).toBeFalsy();
      expect(isDefined('')).toBeFalsy();
    });

    it('isDate requires text-mask mask', () => {
      expect(isDate('4', [/\d/])).toBeTruthy();
    });
  });
});
