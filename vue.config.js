var path = require('path');

module.exports = {
  configureWebpack: {
    entry: {
      worker: './src/lib/idb.worker.js'
    }
  }
};
