const path = require('path');

const webpack = require('webpack');

const distFolder = './public';

module.exports = (env, argv) => {
  const publicPath = '/';
  return {
    entry: {
      'db-updater': './src/lib/idb.worker.js'
    },
    output: {
      path: path.resolve(__dirname, distFolder),
      publicPath: publicPath,
      filename: '[name].js'
    },
    module: {
      rules: [
        /*
        {
          enforce: 'pre',
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
          options: {
            failOnWarning: true,
            failOnError: false
          }
        }, 
        */
        {
          resolve: {
            extensions: ['.js']
          },
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        }
      ]
    },
    plugins: [
      new webpack.ProvidePlugin({
        Map: 'core-js/es6/map', 
        WeakMap: 'core-js/es6/weak-map', 
        Promise: 'core-js/es6/promise', 
        regeneratorRuntime: 'regenerator-runtime' // to support await/async syntax
      })
    ]
  }
};
