import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import IDBStorage from './lib/IDBStorage';
import IDBStoragePlugin from './lib/plugins/VueIDBStorage';

Vue.config.productionTip = false;

Vue.use(IDBStoragePlugin);

const initApp = async () => {
  const storage = await IDBStorage.getInstance();
  new Vue({
    router,
    store,
    IDBStorage: storage,
    render: h => h(App)
  }).$mount('#app');
};

initApp().then(() => {
  if ('serviceWorker' in navigator) {
    /*
    navigator.serviceWorker.register('service-workers.js', { scope: '/' });
      .then(registration => {
        console.log('worker registered ', registration, registration.scope);
      })
      .catch(err => {
        console.log('worker registration failed ', err);
      });
    navigator.serviceWorker.register('db-updater.js', { scope: '/' });
    */
  }
});
