import Vue from 'vue';
import Router from 'vue-router';
import ListView from './views/ListView.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ListView',
      component: ListView
    },
    {
      path: '/add',
      name: 'AddExpenditureView',
      // route level code-splitting this generates a separate chunk (about.[hash].js)
      // for this route which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "addView" */ './views/AddExpenditureView.vue')
    }
  ]
});
