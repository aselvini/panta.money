import IDBStorage from './IDBStorage';
import config from '../config';

let storage;

self.addEventListener('install', event => {
  const setupIDB = async () => {
    storage = await IDBStorage.init('outlays', config.db.schema.entries);
    self.skipWaiting();
  };
  event.waitUntil(setupIDB());
});

self.addEventListener('activate', event => {
  console.log('+++ [idb.worker] activated ', storage);

  setInterval(dumpEntries, 5000);
});

self.onmessage = event => {
  console.log('onmessage ', event);
};

const dumpEntries = () => {
  const transaction = storage.transaction(['entries']);
  const objectStore = transaction.objectStore('entries');
  const entries = [];
  objectStore.openCursor().onsuccess = (event) => {
    const cursor = event.target.result;
    if (cursor) {
      entries.push(cursor.value);
      cursor.continue();
    } else {
      console.log('entries found: ', entries);
    }
  };
  objectStore.openCursor.onfailure = (event) => {
    console.log('get failed: ', event);
  };
};
