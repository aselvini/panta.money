let Vue;
// The name is quite common and I'm still unsure.
// This is the name used in the Vue instance, is how it works considering Vuex instance.
const instanceName = 'IDBStorage';

/* Vue helpers */
const install = function (_Vue) {
  if (Vue && _Vue === Vue) {
    if (process.env.NODE_ENV !== 'production') {
      console.error(
        `[${instanceName}] already installed. Vue.use(${instanceName}) should be called only once.`
      );
    }
    return;
  }
  Vue = _Vue;
  Vue.mixin({ beforeCreate: vueDbInit });
};

function vueDbInit () {
  const options = this.$options;
  const $instanceName = '$' + instanceName;

  // db injection
  if (options[instanceName]) {
    this[$instanceName] = typeof options.db === 'function'
      ? options[instanceName]()
      : options[instanceName];
  } else if (options.parent && options.parent[$instanceName]) {
    this[$instanceName] = options.parent[$instanceName];
  }
}

export default {
  install
};
