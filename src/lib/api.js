import { fetch } from 'whatwg-fetch';

export const login = () => {
  const fakeAuth = {
    username: 'angelo',
    password: 'test'
  };

  fetch('https://services.panta.lan/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: 'include',
    body: JSON.stringify(fakeAuth)
  })
    .then(r => { console.log('response: ', r); return r; })
    .then(r => r.json())
    .then(response => {
      console.log(response);
      // document.cookie=`token=${response.token}; domain=.outlays.panta.lan`;
      localStorage.setItem('token', response.token);
    })
    .catch(err => console.error('something gone wrong', err));
};

export const status = () => {
  fetch('https://services.panta.lan/auth/status', {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('token')
    }
  })
    .then(r => r.json())
    .then(response => console.log('Status response: ', response))
    .catch(err => console.error('Status error:', err));
};

export const getOutlays = ({ msFrom = undefined, msTo = undefined}) => {
  console.log('getOutlays:41 range: ', new Date(msFrom), new Date(msTo) );
  const url = new URL('https://services.panta.lan/outlays/list');
  msFrom && url.searchParams.set('from', msFrom);
  msTo && url.searchParams.set('to', msTo);

  return fetch(url.toString(), {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('token')
    }
  })
    .then(r => r.json());
};

export const addOutlay = (price, currency, timestamp, notes) => {
  return fetch('https://services.panta.lan/outlays/add', {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('token')
    },
    body: JSON.stringify({
      price, currency, timestamp, notes
    })
  });
};
