
// Detect service worker or browser context
const getContext = () => {
  return self || window;
};

if (!getContext().indexedDB) {
  throw new Error('IndexedDB not supported');
}

let instance;

const dbName = 'outlays';

const getInstance = () => {
  if (instance) {
    return instance;
  }

  if (!dbName) {
    throw new Error('To init an indexeddb a name is required');
  }

  instance = new Promise((resolve, reject) => {
    const dbRequest = getContext().indexedDB.open(dbName, 1);

    dbRequest.onsuccess = evt => {
      resolve(evt.target.result);
    };

    dbRequest.onerror = evt => {
      reject(event.target.errorCode);
      throw new Error('DB failed to open; error ', event.target.errorCode);
    };

    dbRequest.onupgradeneeded = evt => {
      const db = evt.target.result;

      db.createObjectStore('entries', { autoIncrement: true });
    };
  });

  return instance;
};

const saveEntry = async (entry) => {
  const idb = await getInstance();
  const transaction = idb.transaction(['entries'], 'readwrite');
  const objectStore = transaction.objectStore('entries');

  objectStore.add(entry);
};

const getEntries = async () => {
  const idb = await getInstance();
  const transaction = idb.transaction(['entries']);
  const objectStore = transaction.objectStore('entries');
  const entries = [];

  return new Promise((resolve, reject) => {
    objectStore.openCursor().onsuccess = (event) => {
      const cursor = event.target.result;
      if (cursor) {
        entries.push(cursor.value);
        cursor.continue();
      }
      resolve(entries);
    };
    objectStore.openCursor.onfailure = (event) => {
      console.log('get failed: ', event);
      reject(event);
    };
  });
};

export default {
  getInstance,
  saveEntry,
  getEntries
};
