const stringToDate = (str, dateSeparator) => {
  const slice = str.split(dateSeparator);
  return new Date(slice[2], parseInt(slice[1], 10) - 1, parseInt(slice[0], 10));
};

const dateToString = (date, dateSeparator) => {
  function zeroFill(num) {
    return (num < 10 ? '0' : '') + num;
  }
  return [zeroFill(date.getDate()), zeroFill(date.getMonth() + 1), date.getFullYear()].join(dateSeparator);
};

const isDefined = (v) => {
  return v !== undefined && v !== null && v !== '';
};

const isDate = (d, mask) => {
  const chars = d.split('');
  return d !== '' && mask.every((rule, index) => {
    return rule instanceof RegExp ?
      mask[index].test(chars[index]) :
      mask[index] === chars[index];
  });
};

const isFilledArray = (a) => {
  return a instanceof Array && a.length > 0;
};

const getFirstDateOfMonth = () => {
  const today = new Date();
  today.setDate(1);
  return today.getTime();
};

const getLastDateOfMonth = () => {
  const today = new Date();
  const month = today.getMonth();
  today.setDate(1);
  today.setMonth(month + 1);
  today.setDate(today.getDate() - 1);
  return today.getTime();
};

const getMonday = (today = new Date()) => {
  return today.setHours(-24 * ((today.getDay() || 7) - 1), 0, 0, 0);
};

const getSunday = () => {
  const today = new Date();
  return today.setHours(24 * (7 - (today.getDay() || 7)), 0, 0, 0);
};

export {
  stringToDate,
  dateToString,
  isDefined,
  isDate,
  isFilledArray,
  getFirstDateOfMonth,
  getLastDateOfMonth,
  getMonday,
  getSunday
};
