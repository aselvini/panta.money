import Vue from 'vue';
import Vuex from 'vuex';
import { addOutlay, getOutlays } from './lib/api';
import IDBStorage from './lib/IDBStorage';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    entries: []
  },
  mutations: {
    setList: (state, list) => {
      state.entries = list;
    }
  },
  actions: {
    saveEntry: async function ({ commit, dispatch }, payload) {
      const { date, groups, price, currency } = payload;
      const outlay = await addOutlay(price, currency, date.getTime(), { categories: groups })
        .catch(err => {
          console.log('Error in save entry: ', err);
          IDBStorage.saveEntry(payload);
        });

      if (outlay && outlay.ok) {
        dispatch('loadEntries');
      }
    },

    loadEntries: async function ({ commit }, payload) {
      const list = await getOutlays(payload)
        .catch(err => {
          console.error('Cannot get list ', err);
        });

      if (list) {
        commit('setList', list);
      }
    }
  }
});
